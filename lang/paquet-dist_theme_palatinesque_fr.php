<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'dist_theme_palatinesque_nom'         => "Thème Dist Palatinesque",
    'dist_theme_palatinesque_slogan'      => "Minimaliste et lisible.",
    'dist_theme_palatinesque_description' => ""
);

?>
